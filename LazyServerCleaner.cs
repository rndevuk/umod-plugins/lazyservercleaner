using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Oxide.Core;
using Oxide.Core.Plugins;
using Oxide.Core.Libraries.Covalence;
using UnityEngine;
using Newtonsoft.Json;

namespace Oxide.Plugins
{
    [Info("LazyServerCleaner", "Ryan Nolan", "1.0.0")]
    [Description("Automatically Clean trashpiles, loot boxes, Dead Bags and other entities after a predetermined time and enable server owners to ban items from being crafted.")]

    public class LazyServerCleaner : CovalencePlugin
    {
        #if RUST
        [PluginReference]
        private Plugin UniversalChatAPI;

        private static ConfigFile _Config;

        private DataFileSystem _DataDir    = new DataFileSystem($"{Interface.Oxide.DataDirectory}\\LazyServerCleaner\\");

        private List<BackpackObj> _Backpacks    = new List<BackpackObj>();
        private List<JunkPileObj> _JunkPiles    = new List<JunkPileObj>();
        private List<ProtectedObj> _Protected   = new List<ProtectedObj>();

        private bool _IsRunning = false;

        #region Umod Hooks
        private void Loaded()
        {
            LoadConfig();

            if (_Config.BannedItems.IsEnabled == true && _Config.BannedItems.AlphaLootRemoval == true && _Config.BannedItems.Items.Count() > 0)
            {
                foreach(string BannedItem in _Config.BannedItems.Items)
                {
                    ConsoleSystem.Run(ConsoleSystem.Option.Server, $"al.itemremove {BannedItem}");
                }
                ConsoleSystem.Run(ConsoleSystem.Option.Server, $"oxide.reload AlphaLoot");
            }

            _Protected = _DataDir.ReadObject<List<ProtectedObj>>("Protected");

            if(_Config.GhostEntities.IsEnabled == true || _Config.JunkPiles.IsEnabled == true || _Config.Backpacks.IsEnabled == true)
            {
                timer.Every(5f, () =>
                {
                    if(_Config.GhostEntities.IsEnabled == true)
                    {
                        foreach(ProtectedObj Protectedobj in _Protected.Distinct().ToList())
                        {
                            if (DateTime.Compare(DateTime.Now, Protectedobj.Date) >= 0)
                                _Protected.Remove(Protectedobj);
                        }
                    }

                    if(_Config.JunkPiles.IsEnabled == true)
                    {
                        foreach(JunkPileObj Junkpileobj in _JunkPiles.Distinct().ToList())
                        {
                            if (DateTime.Compare(DateTime.Now, Junkpileobj.Date) >= 0)
                            {
                                if (Junkpileobj.Junkpile.IsDestroyed == false)
                                    Junkpileobj.Junkpile.TimeOut();

                                _JunkPiles.Remove(Junkpileobj);
                            }
                        }
                    }

                    if(_Config.Backpacks.IsEnabled == true)
                    {
                        foreach(BackpackObj Backpackobj in _Backpacks.Distinct().ToList())
                        {
                            if (DateTime.Compare(DateTime.Now, Backpackobj.Date) >= 0)
                            {
                                if (Backpackobj.Backpack.IsDestroyed == false)
                                    Backpackobj.Backpack.Kill();

                                _Backpacks.Remove(Backpackobj);
                            }
                        }
                    }
                });

                if(_Config.GhostEntities.IsEnabled == true)
                {
                    timer.Every(_Config.GhostEntities.Timer * 60f, () =>
                    {
                        ServerMgr.Instance.StartCoroutine(RemoveGhostEntities());
                    });
                }
            }

            timer.Every(30f, () =>
            {
                _DataDir.WriteObject<List<ProtectedObj>>("Protected", _Protected);
            });
        }

        private object CanCraft(ItemCrafter itemCrafter, ItemBlueprint bp, int amount)
        {
            if (_Config.BannedItems.IsEnabled == true && _Config.BannedItems.Items.Contains(bp.targetItem.shortname))
            {
                BasePlayer player = itemCrafter.GetComponent<BasePlayer>();
                SendMessageToPlayer(player, "BannedItemMessage", bp.targetItem.displayName.english);
                return false;
            }
            return null;
        }

        private void OnEntityDeath(BaseCombatEntity Entity, HitInfo Info)           => AddJunkPile(Entity);

        private void OnLootEntityEnd(BasePlayer Player, BaseCombatEntity Entity)    => AddJunkPile(Entity);

        private void OnEntitySpawned(BaseNetworkable NetEntity)
        {
            BaseEntity Entity = NetEntity as BaseEntity;
            if(Entity == null || Entity.IsDestroyed == true || Entity.OwnerID == 0)
                return;

            if(_Config.GhostEntities.IsEnabled == true)
            {
                if (_Protected.Exists(p => p.Entitynetid == Entity.net.ID) == true)
                    return;

                if (Entity.GetBuildingPrivilege() != null)
                    return;

                DateTime ExpireDate = DateTime.Now.AddMinutes(_Config.GhostEntities.ProtectedTimer);

                _Protected.Add(new ProtectedObj(ExpireDate, Entity.net.ID));
            }

            if(_Config.Backpacks.IsEnabled == true)
            {
                if(Entity.ShortPrefabName != "item_drop_backpack")
                    return;

                if (_Backpacks.Exists(p => p.Entitynetid == Entity.net.ID) == true)
                    return;

                DateTime ExpireDate = DateTime.Now.AddMinutes(_Config.Backpacks.Timer);

                _Backpacks.Add(new BackpackObj(ExpireDate, Entity));
            }
        }
        #endregion

        #region Console Commands
        [ConsoleCommand("lsc_entity_cleanup")]
        void ccmdEntities(ConsoleSystem.Arg arg)
        {
            ServerMgr.Instance.StartCoroutine(RemoveGhostEntities());
        }
        #endregion

        #region Private Methods
        private void AddJunkPile(BaseCombatEntity NetEntity)
        {
            if(_Config.JunkPiles.IsEnabled == false)
                return;

            BaseEntity Entity = NetEntity as BaseEntity;
            if(Entity == null || Entity.IsDestroyed == true || IsLootContainer(Entity) == false)
                return;

            List<JunkPile> JunkPileList = new List<JunkPile>();

            Vis.Entities(Entity.transform.position, 3f, JunkPileList, Rust.Layers.Mask.Default);

            JunkPile Junkpile = JunkPileList.OfType<JunkPile>().FirstOrDefault();

            if (Junkpile == null || Junkpile.IsDestroyed == true || _JunkPiles.Exists(p => p.Entitynetid == Junkpile.net.ID) == true)
                return;

            DateTime ExpireDate = DateTime.Now.AddMinutes(_Config.JunkPiles.Timer);

            _JunkPiles.Add(new JunkPileObj(ExpireDate, Junkpile));
        }

        private bool IsLootContainer(BaseEntity Entity)
        {
            string Ent = Entity.GetType().ToString();
            return Ent == "LootContainer" ? true : false;
        }

        private IEnumerator RemoveGhostEntities()
        {
            if (_IsRunning == true)
            {
                ConsoleLog("CleaningIsAlreadyRunning");
                yield break;
            }

            ConsoleLog("CleaningInProgress");
            SendMessageToGlobal("CleaningInProgress");

            _IsRunning          = true;

            int RemovalCount    = 0;

            IEnumerator<BaseNetworkable> Entities = BaseNetworkable.serverEntities.GetEnumerator();

            while(Entities.MoveNext())
            {
                BaseEntity Entity = Entities.Current as BaseEntity;

                if(Entity == null)
                {
                    yield return new WaitForEndOfFrame();
                    continue;
                }

                if(_Config.GhostEntities.Whitelist.Contains(Entity.ShortPrefabName))
                {
                    yield return new WaitForEndOfFrame();
					continue;
                }

                if (_Protected.Exists(p => p.Entitynetid == Entity.net.ID) == true)
                {
                    yield return new WaitForEndOfFrame();
                    continue;
                }

                if(Entity.OwnerID == 0)
                {
                    yield return new WaitForEndOfFrame();
                    continue;
                }

                if(Entity.GetBuildingPrivilege() != null)
                {
                    yield return new WaitForEndOfFrame();
                    continue;
                }

                if(Entity.IsDestroyed)
            	{
            		yield return new WaitForEndOfFrame();
            		continue;
            	}

                LogToFile("entities", $"[{DateTime.Now.ToString("HH':'mm")}] Removed {Entity.ShortPrefabName} at location: {Entity.transform.position}", this);

                RemovalCount++;
                Entity.Kill();

                yield return new WaitForEndOfFrame();
                continue;
            }

            _IsRunning = false;

            if(RemovalCount > 0)
            {
                SendMessageToGlobal((RemovalCount == 1 ? "CleaningCompleted" : "CleaningCompletedPlural"), RemovalCount);
                ConsoleLog((RemovalCount == 1 ? "CleaningCompleted" : "CleaningCompletedPlural"), RemovalCount);
                yield break;
            }

            SendMessageToGlobal("CleaningCompletedEmpty");
            ConsoleLog("CleaningCompletedEmpty");
        }
        #endregion

        #region Config
        protected override void LoadConfig()
        {
            base.LoadConfig();

            _Config = Config.ReadObject<ConfigFile>();

            if (_Config == null)
            {
                _Config = new ConfigFile
                {

                    BannedItems = new ConfigBannedItems
                    {
                        IsEnabled           = false,
                        AlphaLootRemoval    = false,
                        Items               = new List<string>
                        {
                            "spinner.wheel"
                        }
                    },
                    JunkPiles = new ConfigJunkPiles
                    {
                        IsEnabled   = true,
                        Timer       = 1f,
                    },
                    Backpacks = new ConfigBackpacks
                    {
                        IsEnabled   = true,
                        Timer       = 10f,
                    },
                    GhostEntities = new ConfigGhostEntities
                    {
                        IsEnabled       = true,
                        Timer           = 30f,
                        ProtectedTimer  = 10f,
                        Whitelist       = new List<string>
                        {
                            "stash.small",
                            "small_stash_deployed",
                            "minicopter.entity"// Cleanup was removing player spawned minicopters(spawned via mods)
                        }
                    }
                };

                SaveConfig();
            }
        }

        protected override void SaveConfig() => Config.WriteObject(_Config);

        private class ConfigGhostEntities
        {
            [JsonProperty("Enabled")]
            public bool IsEnabled;
            [JsonProperty("Run Frequency (minutes)")]
            public float Timer;
            [JsonProperty("Protection Timer (minutes)")]
            public float ProtectedTimer;
            [JsonProperty("Whitelist (Entity shortnames to be protected from cleanup)")]
            public List<string> Whitelist;
        }

        private class ConfigJunkPiles
        {
            [JsonProperty("Enabled")]
            public bool IsEnabled;
            [JsonProperty("Timer (minutes)")]
            public float Timer;
        }

        private class ConfigBackpacks
        {
            [JsonProperty("Enabled")]
            public bool IsEnabled;
            [JsonProperty("Timer (minutes)")]
            public float Timer;
        }

        private class ConfigBannedItems
        {
            [JsonProperty("Enabled")]
            public bool IsEnabled;
            [JsonProperty("AlphaLootRemoval")]
            public bool AlphaLootRemoval;
            [JsonProperty("Items")]
            public List<string> Items;
        }

        private class ConfigFile
        {
            [JsonProperty("BannedItems")]
            public ConfigBannedItems BannedItems;

            [JsonProperty("JunkPiles")]
            public ConfigJunkPiles JunkPiles;

            [JsonProperty("Backpacks")]
            public ConfigBackpacks Backpacks;

            [JsonProperty("Entities")]
            public ConfigGhostEntities GhostEntities;
        }

        #endregion

        #region Language
        protected override void LoadDefaultMessages()
        {
            lang.RegisterMessages(new Dictionary<string, string>
            {
                ["CleaningInProgress"]          = "Server cleaning in progress, expect some lag",
                ["CleaningCompleted"]           = "Cleaning completed. Thank you for your patience, We removed <color=#00FF5D>{0}</color> entity",
                ["CleaningCompletedPlural"]     = "Cleaning completed. Thank you for your patience, We removed <color=#00FF5D>{0}</color> entities",
                ["CleaningCompletedEmpty"]      = "Cleaning completed. Thank you for your patience",
                ["CleaningStartingSoon"]        = "Cleaning starts in <color=#00FF5D>5 Minutes</color>, Make sure all your buildings and structures are protected by a cupboard!",
                ["ConfigWarningRunFrequency"]   = "You should increase the 'Run Frequency' Config setting to 30 minutes or more, Entity Cleanup can be fairly intensive depending on how many entities are on your map",
                ["CleaningIsAlreadyRunning"]    = "Cleaning is already running, Skipping this clean up session while we wait for the current clean up session to complete",
                ["BannedItemMessage"]          = "You can not craft {0} on this server.",
            }, this);
        }
        #endregion

        private void SendMessageToGlobal(string message, params object[] args)
        {
            if (UniversalChatAPI == null)
            {
                SendMessageToGlobal("lazyLootCleaner", 0, message, args);

                return;
            }

            UniversalChatAPI?.Call("SendMessageToGlobal", this, message, args);
        }

        private void SendMessageToGlobal(string prefix, ulong avatarid, string message, params object[] args)
        {
            if (UniversalChatAPI == null)
            {
                foreach (IPlayer player in players.Connected)
                {
                    SendMessageToPlayer(player, prefix, avatarid, message, args);
                }

                return;
            }

            UniversalChatAPI?.Call("SendMessageToGlobal", this, prefix, avatarid, message, args);
        }

        private void SendMessageToPlayer(IPlayer player, string message, params object[] args)
        {
            if (UniversalChatAPI == null)
            {
                SendMessageToPlayer(player, "lazyLootCleaner", 0, message, args);

                return;
            }

            UniversalChatAPI?.Call("SendMessageToPlayer", this, player, message, args);
        }

        private void SendMessageToPlayer(IPlayer player, string prefix, ulong avatarid, string message, params object[] args)
        {
            if (UniversalChatAPI == null)
            {
                if (string.IsNullOrEmpty(prefix))
                {
                    player.Message(message, null, args);
                }
                else
                {
                    player.Message(message, prefix+":", args);
                }

                return;
            }

            UniversalChatAPI?.Call("SendMessageToPlayer", this, player, prefix, avatarid, message, args);
        }

        private void SendMessageToPlayer(BasePlayer player, string message, params object[] args)
        {
            if (UniversalChatAPI == null)
            {
                SendMessageToPlayer(player, "lazyLootCleaner", 0, message, args);

                return;
            }

            UniversalChatAPI?.Call("SendMessageToPlayer", this, player, message, args);
        }

        private void SendMessageToPlayer(BasePlayer player, string prefix, ulong avatarid, string message, params object[] args)
        {
            if (UniversalChatAPI == null)
            {
                if (args.Length > 0)
                    message = string.Format(message, args);

                if (!string.IsNullOrEmpty(prefix))
                    message = string.Format("{0}: {1}", prefix, message);

                player.SendConsoleCommand("chat.add", 2, avatarid, Formatter.ToUnity(message));

                return;
            }

            UniversalChatAPI?.Call("SendMessageToPlayer", this, player, prefix, avatarid, message, args);
        }

        private void ConsoleLog(string key, params object[] args)
		{
            string RawMsg       = lang.GetMessage(key, this, null);
            string FormatedMsg  = string.Format(RawMsg, args);
            string CleanedMsg   = Regex.Replace(FormatedMsg, @"<(.|\n)*?>", String.Empty);

            Log(CleanedMsg);
        }

        private class BackpackObj
        {
            public DateTime Date;
            public BaseEntity Backpack;
            public uint Entitynetid;

            public BackpackObj(DateTime Date, BaseEntity Backpack)
            {
                this.Date           = Date;
                this.Backpack       = Backpack;
                this.Entitynetid    = Backpack.net.ID;
            }
        }

        private class JunkPileObj
        {
            public DateTime Date;
            public JunkPile Junkpile;
            public uint Entitynetid;

            public JunkPileObj(DateTime Date, JunkPile Junkpile)
            {
                    this.Date           = Date;
                    this.Junkpile       = Junkpile;
                    this.Entitynetid    = Junkpile.net.ID;
            }
        }

        private class ProtectedObj
        {
            public DateTime Date;
            public uint Entitynetid;

            public ProtectedObj(DateTime Date, uint Entitynetid)
            {
                    this.Date           = Date;
                    this.Entitynetid    = Entitynetid;
            }
        }

        #else
        private void Loaded()
        {
            LogError("LazyServerCleaner only works with Rust");
        }
        #endif
    }
}
